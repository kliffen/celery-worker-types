FROM python:3.10-slim

RUN pip install --upgrade pip && \
    pip install --no-cache-dir pipenv

WORKDIR /tmp
COPY Pipfile* ./
RUN pipenv lock --requirements >> /tmp/requirements.txt && \
    pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR /app
COPY taskserver ./

EXPOSE 8000
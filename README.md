# Celery Worker Types

Test application for different celery worker types

Pre-fork: https://docs.celeryproject.org/en/stable/userguide/workers.html#concurrency
Eventlet: https://docs.celeryproject.org/en/stable/userguide/concurrency/eventlet.html#concurrency-eventlet

## Running the tests
**TODO**

## Findings

### Pre-fork
`<num cpus>` threads by default (configurable by --concurrency)
If threads are exhausted, worker does not take on new jobs until current jobs are finished.
Possibly the pool could be exhausted by long running tasks, even if they are sleeping!

This could be handy in cases where the process is limited by processing power and should not
take on extra work.

### Eventlet
Eventlet is able to have more active tasks (tested with 1000) running on a single node.
They should not be processing intensive tasks and should be compatible with the Eventlet library.
**TODO** Check PYVO and requests libraries
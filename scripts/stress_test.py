#!/usr/bin/env python3

""" Stress test the application """

import requests

for i in range(1000):
    try:

        data = {"task_id": i}

        res = requests.post("http://localhost:8000/uws/long/", json=data)
    except Exception as ex:
        print("An error occurred!")
        print(ex)

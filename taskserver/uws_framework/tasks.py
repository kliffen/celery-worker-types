import logging
import time

from celery import shared_task

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@shared_task
def short_task(task_id: int):
    logger.info(f"Short {task_id}started")
    time.sleep(1)
    logger.info(f"Short {task_id} ended")


@shared_task
def long_task(task_id: int):
    logger.info(f"Long {task_id} started")
    time.sleep(10)
    logger.info(f"Long {task_id} ended")

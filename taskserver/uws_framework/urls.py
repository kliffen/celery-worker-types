from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("long/", views.long, name="long"),
    path("short/", views.short, name="short"),
]

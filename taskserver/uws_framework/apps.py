from django.apps import AppConfig


class UwsFrameworkConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uws_framework'

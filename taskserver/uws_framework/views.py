import json
from http import HTTPStatus

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from uws_framework.tasks import long_task, short_task


class HttpCreated(HttpResponse):
    status_code = HTTPStatus.ACCEPTED


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


@require_http_methods(["POST"])
@csrf_exempt
def short(request):
    data = json.loads(request.body)
    short_task.delay(data["task_id"])
    return HttpCreated()

@require_http_methods(["POST"])
@csrf_exempt
def long(request):
    data = json.loads(request.body)
    long_task.delay(data["task_id"])
    return HttpCreated()
